[[!meta title="Iteration planning note: June 20"]]
[[!tag meeting]]
[[!meta date="2020-06-20 18:00"]]

# What has happened?

- We got quite a bit done, but not everything yet.
- Daniel has pushed an MR for issue 12 but Lars hasn't had time to
  go over it until next iteration.
- We're not making rapid progress, but we're both very busy with other things
  right now and that's fine.

So we have:

- One outstanding MR ([[!mr 54]])
- One untackled issue ([[!issue 20]])
- No branches which need cleaning up

# Issue review

We reviewed the situation for all the open issues, for the most part nothing
was changed, however the following tweaks were done:

- We added the `typesetting` label to [[!issue 27]]
- Lars made a note in [[!issue 48]] that we're letting multiple binding/function
  files bed-in before we try making any more decisions about libraries.
- We discussed [[!issue 62]] because Lars had made some comments:
  - We think 'integral commits' is a good way to say what we mean.
  - We're not going to mandate 'conventional commits'
  - Lars labelled it active-discussion
  - We removed it from the iteration.
- Added `actively-discussed` label to [[!issue 65]]

We agreed to continue with our current issue-filing and closing/keeping flow
the same for now.

# Milestone planning

[[!milestone 10]] has two issues in it, both assigned to Daniel. Lars needs
to review [[!mr 54]] which is for [[!issue 12]], and Daniel needs to write an MR
for [[!issue 20]].

# Mainline branch name

Daniel was concerned that we might be virtue signalling, but also acknowledges
that he's not sure it's not worth it. We're happy to make the change, though
we don't think it's high priority necessarily. OTOH subplot doesn't have a huge
number of collaborators, so a change now would be low cost.

Daniel created [[!issue 68]] assigned it to himself and put it into the milestone.

# Actions

- Lars to review [[!mr 54]]
- Daniel to work on a solution for [[!issue 20]]
