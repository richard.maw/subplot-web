[[!meta title="Iteration planning notes: February 15"]]
[[!tag meeting]]
[[!meta date="2020-02-15 18:35"]]

What has happened
=============================================================================

* Only a week since the previous meeting.

* Lars has fixed some issues. One remains from the set planned for
  this iteration: adding shell support to codegen.

* Lars has additionally talked with people at work about using Subplot
  for two different internal tools. Initial reactions are positive,
  but nothing has actually happened yet, due to other, pressing issues.

Discussion
=============================================================================

* We discussed the shell support issue. It's larger than we
  anticipated. We decided to drop it from the iteration.

* We decided to not make a formal release before asking for
  volunteers. A git tag will be enough.

* We decided we're ready to ask for volunteers. Lars will publish a
  blog post, and handle the support load while Daniel is away during
  the next iteration.

* We discussed new issues, all filed by Lars. Most are trivial, some
  will require a bit of work. We decided to add a `--set-date=FOO`
  option, and that Subplot will use the mtime of the markdown input
  file if the option isn't used.

Actions
=============================================================================

* Lars to publish blog post requesting two volunteers to try Subplot.
